#!/usr/bin/env python3

import matplotlib
import matplotlib.pyplot as plt
import numpy as np
import os
import wave

from scipy import signal
from scipy.fft import fftshift


if __name__ == "__main__":
    directory = input("Insert directory: ")

    # matplotlib.use("pgf")
    # matplotlib.rcParams.update({
    #             "pgf.texsystem": "pdflatex",
    #             'font.family': 'serif',
    #             'text.usetex': True,
    #             'pgf.rcfonts': False,
    # })

    for root, dirs, files in os.walk(directory):
        for file in files:
            spf = wave.open(os.path.join(root, file), "r")

            filename, extension = os.path.splitext(file)
            print(filename)

            # Extract Raw Audio from Wav File
            waveform = spf.readframes(-1)
            waveform = np.frombuffer(waveform, np.int16)

            f, t, Sxx = signal.spectrogram(waveform, spf.getframerate())

            T, F = np.meshgrid(t, f)


            waveform = waveform[0:spf.getframerate()]

            lenght = 20
            T = T[:, 0:lenght]
            F = F[:, 0:lenght]
            Sxx = Sxx[:, 0:lenght]

            plt.figure(1)
            plt.title("Waveform of " + filename)
            plt.plot(waveform)

            plt.figure(2)
            ax = plt.axes(projection = "3d")
            ax.plot_surface(T, F, np.log(Sxx))
            plt.title("Spectrum of " + filename)
            plt.ylabel('Frequency [Hz]')
            plt.xlabel('Time [sec]')
            plt.show()

            # saved_figure_name = "spectrum" + filename + ".pgf"
            # print(saved_figure_name)
            # plt.savefig(saved_figure_name)
